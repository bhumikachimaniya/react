# REACTS....
 
## what is react ?

React.js is an open-source JavaScript library that is used for building user interfaces specifically for single-page applications. It’s used for handling the view layer for web and mobile apps. React also allows us to create reusable UI components. React was first created by Jordan Walke, a software engineer working for Facebook.

 React is a **js** libary for building a user interfaces (UI).

 it is based on UI component.

React is a JavaScript library created by facebook.

React is a tool for building UI components.

**how the app looks and feels**

dedicated tools for easy debugging 

**" In react, render is a method that tell react what to display."** 

## " react application are component "

component is essentially a piece of a user interfaces (UI).

every react application has at least one components which we refers to as the roots components .

# A Brief History of React.js
Facebook created React.js in 2011 for their own use. As you know, Facebook is one of the biggest Social Networking websites in the world today.

In 2012, Instagram also started using it, which is a subsidiary of Facebook.

In 2013, Facebook made React.js open-sourced. Initially, the developer community rejected it because it used Markup and JavaScript in a single file. But as more people experimented with it, they started embracing the component-centric approach for separating concerns.

In 2014, many large companies started using React.js in their production environments.

In 2015, Facebook open-sourced React Native too. It is a library that allows us to create native mobile apps in Android and iOS using React.js.

In 2016, with version 15, React.js started using Semantic Versioning. This also helped convey to the developer's community that React was more stable.

Today, React.js is used by many Fortune 500 companies. Facebook has full-time React development staff. They regularly release bug fixes, enhancements, blog posts, and documentation.

# Why Should You Use React?

* **flexible** :- React was created with a single focus: to create components for web applications. A React component can be anything in your web application like a Button, Text, Label, or Grid.

* React also supports server rendering of its components using tools like Next.js. You can also use React.js to create a virtual reality website and 360 experiences using React VR.

* The main advantage of using a Library over a Framework is that Libraries are lightweight, and there is a freedom to choose different tools. The Framework consists of an entire ecosystem to build an application, and you don't have an easy way to use any other tools outside the Framework."

# Why is react called SPA?

js from where your entire web-app is loaded in fragments and components. This behaviour of rendering components and pages on a single page and changing the DOM( is a single page behaviour and hence the name), instead of loading a new page with new content, this makes it feel like a single application. 

# what is virtual dom ?

js from where your entire web-app is loaded in fragments and components. This behaviour of rendering components and pages on a single page and changing the DOM( is a single page behaviour and hence the name), instead of loading a new page with new content, this makes it feel like a single application.

A virtual DOM object has the same properties as a real DOM object, but it lacks the real thing’s power to directly change what’s on the screen.

Manipulating the DOM is slow. Manipulating the virtual DOM is much faster, because nothing gets drawn onscreen. Think of manipulating the virtual DOM as editing a blueprint, as opposed to moving rooms in an actual house.

## feactures of react ----

1. jsx

1. virtual dom

1. one way data binding

1. extensions

1. debugging 

--------

1. **jsx** :- 

   js + html = jsx 


1. **virtual dom** :-

   structure of webpages in a form a tree.

   react creat a virtual dom which is copy of real dom.
    
1. **one way data binding** :-

   keeps everything modular and fast ,when information is displaced and not updated.

1. **extensions** :-

* react native 
* react flux 
* debugging .

1. **component, state & props** :- 

*  **component** :- component are the building blocks of any react application and single app usaually consist of multiple components or it is a piece of user interfaces / codes.

* **state** :- it is a object that hold a some data .

* **props** :-props is short for properties that allow us to pass arugument or data to components .

## company which uses reacts 
 
 1. Insta.
 1. Fb.
 1. whatsapp.

## What is a React single page application?

Single Page Application (SPA) is a web application that does not use the default method of loading new pages completely. Instead, it takes new data from the webserver by interacting with the web browser and refreshes the current web page.

examples are :- facebook , gmail.

## why immutability is important in Javascript ?

One of the advantages of immutability is that you can optimize your application by making use of reference and value equality. This makes it easy to identify if anything has changed. You can consider the example of state change in the React component.

# why is react better ?

In summary, React JS is better than Angular or Vue JS because of its superior Virtual DOM capabilities, its robust community support, rich documentation, its light-weight attributes, manageable learning curve, and its flexibility to allow mobile functionality with React Native's

---------------
# #learning 

### 1. how react works internally ?

* react basically maintain a tree for you.

* this tree is do efficent diff computations the nodes .

* react allow you to re-construct you DOM with changes to the dom . 

* which have actually occured with the help of virtual DOM.

 **JSX IN REACT**

 convert HTMLto JS object.

           const tag = <h1>Hello</h1>


what you're essentially doing is this:

           const tag = React.createElement("h1", {}, "Hello")

**How does it work** : While building client-side apps, a team of Facebook developers realized that the DOM is slow (The Document Object Model (DOM) is an application programming interface (API) for HTML and XML documents. It defines the logical structure of documents and the way a document is accessed and manipulated.). So, to make it faster, React implements a virtual DOM that is basically a DOM tree representation in JavaScript. So when it needs to read or write to the DOM, it will use the virtual representation of it. Then the virtual DOM will try to find the most efficient way to update the browser’s DOM.
Unlike browser DOM elements, React elements are plain objects and are cheap to create. React DOM takes care of updating the DOM to match the React elements. The reason for this is that JavaScript is very fast and it’s worth keeping a DOM tree in it to speed up its manipulation.
Although React was conceived to be used in the browser, because of its design it can also be used in the server with Node.js. 
 
### 2. how react hosts the websites. ?

### 3. what is webpack ?

Webpack is a tool that lets you compile JavaScript modules, also known as module bundler. Given a large number of files, it generates a single file (or a few files) that run your app. It can perform many operations: helps you bundle your resources. watches for changes and re-runs the tasks.

Webpack is not limited to be use on the frontend, it’s also useful in backend Node.js development as well.

**Webpack works in production mode** :-  I found that in production build we can able to reduce the size of overall code. Currently webpack builds around 8MB files and main. js around 5MB.

**The entry point**
By default the entry point is ./src/index.js This simple example uses the ./index.js file as a starting point

**The output**
By default the output is generated in ./dist/main.js. This example puts the output bundle into app.js:
**Loaders**
Using webpack allows you to use import statements in your JavaScript code to not just include other JavaScript, but any kind of file, for example CSS.

Webpack aims to handle all our dependencies, not just JavaScript, and loaders are one way to do that

**Plugins**
Plugins are like loaders, but on steroids. They can do things that loaders can’t do, and they are the main building block of webpack.

### 4. what is SPA application ?

A single page application (SPA) is a JavaScript framework for distributing application functionality over the web.Single page application (SPA) is a single page (hence the name) where a lot of information stays the same and only a few pieces need to be updated at a time.

 The three most popular SPA tools are:

* Angular: developed by Google and now almost ten years old);

* React: created by Facebook a few years later;

* Vue: developed by an ex-Google employee and which has enjoyed a surge in popularity over the past few years.

For example, when you browser through your email you’ll notice that not much changes during navigation — the sidebar and header remain untouched as you go through your inbox.

The SPA only sends what you need with each click, and your browser renders that information. This is different than a traditional page load where the server re-renders a full page with every click you make and sends it to your browser.

This piece-by-piece, client-side method makes load time much faster for users and makes the amount of information a server has to send a lot less and a lot more cost efficient. A win-win.

With single page applications, presentation logic is rendered on the client side and the visual structure of the web app remains consistent for the duration of the session, with newly requested data being refreshed in the background. This provides a much smoother experience to the user.

###### Advantages of Single Page Applications

1. Loading speed
1. Easy to debug
1. Can be distributed as progressive web app
1. Straightforward development

##### The examples of SPA is ---

Some examples of Single Page Applications are **Gmail**, **Google Maps**, AirBNB, **Netflix**, **Pinterest**, Paypal, and many more. Companies are using SPAs to build a fluid, scalable experience.

### 5. What is functional component programming ?

**A React functional component is a simple JavaScript function that accepts props and returns a React element. After the introduction of React Hooks, writing functional components has become the ​standard way of writing React components in modern applications.**

Functional components are some of the more common components that will come across while working in React. These are simply JavaScript functions. We can create a functional component to React by writing a JavaScript function. These functions may or may not receive data as parameters. In the functional Components, the return value is the JSX code to render to the DOM tree.

Functional components lack a significant amount of features as compared to class-based components. The gap is made up with the help of a special ReactJS concept called **“hooks”**. Hooks are special functions that allow ReactJS features to be used in functional components. 

Functional components do not have access to dedicated state variables like class-based components. The only “state” that a functional component effectively has access to are the props passed to it from its parent component. ReactJS has access to a special hook called **useState()** that can be used for giving the illusion of working with the state in functional components. The useState() is used to initialize only one state variable to initialize multiple state variables, multiple useState() declarations are required. The first value returned is the initial value of the state variable, while the second value returned is a reference to the function that updates it. When the state variable needs to be updated, it can be done by calling the update function and by updating the state variable directly inside it.

#### Why use functional components in React?

The core purpose of a React component is to define the **displayed view and bind** it to the code that drives its behavior. React's functional components distill this down to the simplest possible profile: a function that receives properties and returns a JSX definition.

### 6. what is hooks ?

Hooks are functions that let you " hooks into" React state and lifecycle feactures from function components.

* Hooks allow you to use reacts without classes. its means you can state and other reacts feactures without writing a class.

* react provides a few built in hooks likes state , useffect etc.

* hooks are a new addition in reacts 16.8.

##### when uses a hooks ----

if you write a function component and realize you need to add some state to it 

##### common used reacts hooks 

* use state :-we can updates the state and check the intial values.

* use effect

* use context

* use Ref 

### 7.what is style framework which is supported by react.?

The react MD – a React JS UI Framework, is used to build steady designs for the project, using the react user interface component libraries. It also provides an option to work with the SaaS platforms using the relevant React Components.

































